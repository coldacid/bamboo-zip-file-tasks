package com.pronetbeans.bamboo.filezip;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.utils.FileVisitor;
import java.io.File;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;

public class ZipFileVisitor extends FileVisitor {

    private File myRootDirectory = null;
    private static final Logger log = Logger.getLogger(ZipFileVisitor.class);
    private BuildLogger buildLogger;
    private ZipOutputStream out;
    private String propsZipLocation;

    /**
     * Visit file in the root directory
     *
     * @param rootDirectory
     */
    protected ZipFileVisitor(File rootDirectory) {
        super(rootDirectory);
        this.myRootDirectory = rootDirectory;
    }

    @Override
    public void visitFilesThatMatch(String filePattern) throws InterruptedException {

        String[] patterns = filePattern.split(",");
        for (String pattern1 : patterns) {
            if (Thread.currentThread().isInterrupted()) {
                throw new InterruptedException();
            }
            String pattern = StringUtils.trimToEmpty(pattern1);
            File patterFile = new File(myRootDirectory, pattern);
            if (patterFile.isDirectory()) {
                visitAllFilesInADirectory(patterFile);
            } else {
                visitAllFilesThatMatchAPattern(pattern);
            }
        }
    }

    public void visitAllFilesInADirectory(File patterFile) throws InterruptedException {
        File[] files = patterFile.listFiles();
        if (files != null) {
            for (File file : files) {
                visitFile(file);
            }
        }
    }

    /**
     *
     * @param pattern
     * @throws InterruptedException
     */
    public void visitAllFilesThatMatchAPattern(String pattern) throws InterruptedException {

        FileSet fileSet = new FileSet();
        fileSet.setDir(myRootDirectory);

        // ensure the ZIP file being created is EXCLUDED if it is under the same src working directory.
        // Otherwise the ZIP created is added by into the ZIP file recursively until your
        // disk is full

        PatternSet.NameEntry exclude2 = fileSet.createExclude();
        exclude2.setName(propsZipLocation);

        // exclude the "build-number.txt" fiel that is automatically in each build dir (this is a 
        // file used by Bamboo
        PatternSet.NameEntry exclude = fileSet.createExclude();
        exclude.setName("build-number.txt");

        PatternSet.NameEntry include = fileSet.createInclude();
        include.setName(pattern);

        DirectoryScanner ds = fileSet.getDirectoryScanner(new Project());
        String[] srcFiles = ds.getIncludedFiles();

        for (String srcFile : srcFiles) {
            visitFile(new File(myRootDirectory, srcFile));
        }

        String[] srcDirs = ds.getIncludedDirectories();

        for (String srcDir : srcDirs) {
            visitFile(new File(myRootDirectory, srcDir));
        }
    }

    @Override
    public void visitFile(File file) throws InterruptedException {

        if (file.isDirectory()) {

            try {
                // Add ZIP entry to output stream.
                //out.putNextEntry(new ZipEntry(file.getName()));

            } catch (Exception e) {
                log.info(Utils.getLogBanner());
                buildLogger.addErrorLogEntry("Error adding zip entry : " + e.getMessage(), e);
            }

        } else if (file.isFile()) {
            try {

                final String filePath = file.getAbsolutePath();

                FileInputStream in = new FileInputStream(filePath);
                String trunNameBelowWorkingDir = filePath.substring((int) myRootDirectory.getAbsolutePath().length() + 1);

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(trunNameBelowWorkingDir));

                // Transfer bytes from the file to the ZIP file
                byte[] buf = new byte[1024];

                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                // Complete the entry
                out.closeEntry();
                in.close();

            } catch (Exception e) {
                log.info(Utils.getLogBanner());
                buildLogger.addErrorLogEntry("Failed to add file : " + e.getMessage(), e);
            }
        }
    }

    /**
     * @return the buildLogger
     */
    public BuildLogger getBuildLogger() {
        return buildLogger;
    }

    /**
     * @param buildLogger the buildLogger to set
     */
    public void setBuildLogger(BuildLogger buildLogger) {
        this.buildLogger = buildLogger;
    }

    /**
     * @param out the out to set
     */
    public void setOut(ZipOutputStream out) {
        this.out = out;
    }

    /**
     * @param propsZipLocation the propsZipLocation to set
     */
    public void setPropsZipLocation(String propsZipLocation) {
        this.propsZipLocation = propsZipLocation;
    }
}
