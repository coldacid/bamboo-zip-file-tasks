[@ww.textfield labelKey="Zip File Path & Name" name="propsZipLocation" required='true'/]
[@ww.textfield labelKey="File Pattern to Include" name="propsFilePattern" required='true'/]
[@ww.select id="compressionLevel" labelKey='Compression Level' name='compressionLevel' list="{'0 (None)','1 (Low)','2', '3', '4', '5', '6', '7','8','9 (High)'}" emptyOption="false" /]